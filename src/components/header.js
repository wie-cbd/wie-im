import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import wie from "../images/wie.svg"
console.log(wie)
const Header = ({ siteTitle }) => (
  <header>
    <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `1.45rem 1.0875rem`,
        textAlign: `center`
      }}
    > 
        <Link
          to="/"
          style={{
            color: `black`,
            textDecoration: `none`,
          }}
        > 
        <img style={{maxWidth: `200px`, marginBottom: `2rem`}} src={wie} alt="Logo" />
        </Link> 
        <div id="navcontainer">  
  <ul>
    <li className="bordered">   
      <Link to="/">
          ABOUT
        </Link>
    </li>
    <li className="bordered">
      <Link to="/work">
          WORKS
        </Link>
    </li>
    <li>
      <Link to="/contact">
          CONTACT
        </Link>
    </li>
  </ul>
</div>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
