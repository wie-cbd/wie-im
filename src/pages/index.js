import React from "react" 

import Layout from "../components/layout" 
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Wie - About" keywords={[`Wie`, `Web Developer`, `Melbourne Web Developer`, `Melbourne Front End Developer`]} />
    <h3>ABOUT</h3>
    <p>
      Hello, you can call me Wie. 
    </p> 
    <p>
    I'm a front end developer and UI/UX designer based in Melbourne with a passion for designing and building clean, beautiful websites and web apps.
    </p>
    <p>
    I am experienced in:
    </p>
    <p>
      Angular | React | JavaScript | TypeScript | PHP | HTML 5 | CSS | SQL</p>
  </Layout>
)

export default IndexPage
