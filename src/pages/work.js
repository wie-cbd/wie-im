import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
let works = [
    {
        title   : "CanWork",
        url     : "https://CanWork.io",
        style   : 
                { 
                    backgroundImage: 'url("https://www.canwork.io/assets/img/logo.svg")',
                }
    },
    {
        title   : "CanYa",
        url     : "https://CanYa.io",
        style   : 
            { 
                backgroundImage: 'url("https://canya.io/assets/img/canya.svg")',
            }
    },
    {
        title   : "Loki.Network",
        url     : "https://Loki.network",        
        style   : 
            { 
                backgroundImage: 'url("https://loki.network/wp-content/uploads/2018/10/logo.svg")',
                backgroundColor: '#1a1a1a'
            }
    },
    {
        title   : "Thorchain.org",
        url     : "https://Thorchain.org",
        style   : 
            { 
                backgroundImage: 'url("https://thorchain.org/assets/img/thorchain-white-text.svg")',
                backgroundColor: '#101921'
            }
    },
    {
        title   : "Skyy.Network",
        url     : "https://Skyy.Network",
        style   : 
        { 
            backgroundImage: 'url("https://skyy.network/assets/img/PNG/Blue/Skyynetwork_Logo_Blue.png")',
        }
    }
]

const workItem = works.map((work) =>

<div className="col-sm-6 text-center " key={work.title}> 
    <div className="workItem" style={work.style}>
        <a href={work.url} target="_blank" rel="noopener noreferrer">
            <div style={{width: "100%", height: "100%"}}>
            </div>
        </a>
    </div> 
</div>
)

const Work = () => (
  <Layout>
    <SEO title="Works" />
    <h3>WORKS</h3>
    <div className="container"> 
        <div className="row">
          {workItem}
        </div>
    </div>
  </Layout>
)

export default Work
