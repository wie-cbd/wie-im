import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"

const Work = () => (
  <Layout>
    <SEO title="Works" />
    <h3>CONTACT</h3>
    <p>
        Want to get in touch? Fill in the form below, and I'll get in touch shortly.
    </p>
    <div className="container">
        <div className="row">
            <div className="col-12">   
             <form method="POST" action="https://formspree.io/wicahya.bagus@gmail.com">
             <input type="text" name="_gotcha" className="form-control contact-input" style={{display:`none`}}></input>
                <input type="email" name="email"  className="form-control contact-input" placeholder="Your email"></input>
                <textarea name="message"  className="form-control  contact-input" placeholder="Your Message"></textarea>
                <button type="submit" className="contact">Send Message</button>
              </form>
            </div>
        </div>
    </div>
    <div>
    </div>
    <p>Or, find me on <a href="https://www.linkedin.com/in/wie-cahya/" rel="noopener noreferrer" target="_blank">LinkedIn</a></p>
  </Layout>
)

export default Work
